package com.wallero.crud.assignment.repositories;

import com.wallero.crud.assignment.models.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoRepository extends JpaRepository<Todo, Long> {
}
