package com.wallero.crud.assignment.dtos;

import com.wallero.crud.assignment.models.Todo;
import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TodoRequest {

    @NotBlank
    private String todo;

    public Todo to() {
        return Todo.builder()
                .todo(this.todo)
                .build();
    }
}
