package com.wallero.crud.assignment.controllers;

import com.wallero.crud.assignment.dtos.TodoRequest;
import com.wallero.crud.assignment.models.Todo;
import com.wallero.crud.assignment.services.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1")
public class TodoController {

    @Autowired
    TodoService todoService;

    @PostMapping("/todo/create")
    public Todo createTodo(@RequestBody @Valid TodoRequest todoRequest){
        return todoService.createTodo(todoRequest.to());
    }

    @GetMapping("/todos")
    public List<Todo> getAllTodos() {
        return todoService.getAllTodos();
    }

    @PutMapping("/todo/update/{todoId}")
    private Todo updateTodo(@PathVariable("todoId") Long todoId, @RequestBody @Valid TodoRequest todoRequest)
    {
        return todoService.updateTodo(todoId, todoRequest.to());
    }

    @DeleteMapping("/todo/delete/{todoId}")
    private void deleteTodo(@PathVariable("todoId") Long todoId)
    {
        todoService.deleteTodo(todoId);
    }
}
