package com.wallero.crud.assignment.services;

import com.wallero.crud.assignment.models.Todo;
import com.wallero.crud.assignment.repositories.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class TodoService {

    @Autowired
    TodoRepository todoRepository;

    public Todo createTodo(Todo todo) {
        Todo createdTodo = todoRepository.save(todo);
        return createdTodo;
    }

    public List<Todo> getAllTodos() {
        return todoRepository.findAll();
    }

    public Todo updateTodo(Long todoId, Todo todo) {
        Optional<Todo> todoToUpdateOptional = todoRepository.findById(todoId);

        if (!todoToUpdateOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        Todo todoToUpdate = todoToUpdateOptional.get();
        todoToUpdate.setTodo(todo.getTodo());

        Todo updatedTodo = todoRepository.save(todoToUpdate);
        return updatedTodo;
    }

    public void deleteTodo(Long todoId) {
        Optional<Todo> todoToDeleteOptional = todoRepository.findById(todoId);

        if (!todoToDeleteOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        todoRepository.deleteById(todoId);
    }
}
